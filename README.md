# odoorpc-js-tutorial

### api 接口

1. create a vue project
2. connect to odoo
3. create a database in odoo
4. login odoo
5. dateset call_kw
6. model method
7. use context
8. install module
9. unit-mocha
10. test
11. action, listview, formview 的读取, one2many
12. formview: edit and new, default get and onchange, commit
13. formview del
14. ... next , TBD ...
15. treeview read, many2one and selection
16. formview read, many2many
17. formview edit. many2one and selection, many2many
18. formview edit. one2many
19. formview edit. one2many new
20. formview edit. one2many edit
21. formview edit. one2many del
22. module config

### web 页面

1. 安装 vue-router, 配置项目的 路由
2. api 定义
3. login 页面
4. 定义主页面的 侧边栏, 展示 menu
5. listview 展示列表数据, formview 展示表单数据
6. formview 页面 展示数据 / 编辑数据
7. o2m 展示数据
8. o2m 编辑数据
