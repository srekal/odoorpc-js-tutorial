const subMenus = [{ name: 'tutorial', title: '自定义菜单', icon: 'ios-school' }]

const menuItems = {
  tutorial: [
    { name: 'account.action_move_journal_line', title: '会计凭证' },
    { name: 'base.action_country', title: '国家/地区', readonly: true },
    { name: 'base.action_partner_category_form', title: '业务伙伴类别' },
    { name: 'contacts.action_contacts', title: '业务伙伴' },
    { name: 'account.action_account_moves_all', title: '会计分录' }

    // { name: 'resBank', title: '银行' },
    // { name: 'resPartnerBank', title: '银行账号' },
    // { name: 'resPartnerTitle', title: '头衔' },
    // { name: 'resPartnerIndustry', title: '行业类型' },
    // { name: 'resCountryState', title: '州/省' }
  ]
}

export const routes = Object.keys(menuItems).reduce((acc, cur) => {
  const actions = menuItems[cur].reduce((acc2, menu) => {
    const menu2 = { ...menu }
    delete menu2.title
    acc2[menu.name] = { ...menu2 }
    return acc2
  }, {})
  return { ...acc, ...actions }
}, {})

// console.log(routes)

export const menus = subMenus.map(sub => {
  return { ...sub, children: menuItems[sub.name] }
})

export const get_menus = async () => {
  return menus
}
