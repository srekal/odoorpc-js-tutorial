export class Menu {
  constructor(env, payload = {}) {
    this._env = env
    this._menus = []
  }

  get env() {
    return this._env
  }

  get menus() {
    return this._menus
  }

  async load_menus() {
    const get_root = async () => {
      try {
        const root = await this.env.ref('odoojs_menu.menu_odoojs')
        // console.log(root)
        return root
      } catch {
        return null
      }
    }

    const root = await get_root()

    // console.log('load_menus', root)

    if (!root) {
      return []
    }

    const Model = this.env.model(root.model)
    const domain = [
      '|',
      ['parent_id', '=', root.id],
      ['parent_id.parent_id', '=', root.id]
    ]

    const res = await Model.search_read({ domain })

    // console.log(res)

    const subMenus = res
      .filter(item => item.parent_id[0] === root.id)
      .map(item => {
        const children = res
          .filter(it => it.parent_id[0] === item.id)
          .map(it => {
            const action_id = it.action.split(',')[1]
            return {
              id: it.id,
              name: action_id,
              title: it.name
              //   readonly: true
            }
          })
        return {
          id: item.id,
          name: `ir.ui.menu,${item.id}`,
          title: item.name,
          icon: item.iview_icon,
          children
        }
      })

    // console.log(subMenus)

    this._menus = subMenus

    return subMenus
  }
}
