import { Field } from './model_fields'

import { is_virtual_id } from '../../odoojs/utils'

class MetaModel {
  // constructor() {
  //   //
  // }

  static get metadata() {
    // to be override, to set metadata
    return {}
  }

  static get _name() {
    return this._model
  }

  static get env() {
    return this._env
  }

  get env() {
    return this.constructor.env
  }

  static with_context(context, kwargs = {}) {
    const context2 = context ? context : this.env.context
    const context3 = { ...context2, ...kwargs }
    return this.with_env(this.env.copy(context3))
  }

  static with_env(env) {
    const OldModel = this
    class NewModel extends OldModel {
      constructor(...args) {
        super(...args)
      }
    }

    NewModel._env = env
    return NewModel
  }

  static async execute_kw(method, args = [], kwargs = {}) {
    const kwargs2 = { ...kwargs }

    if (!Object.keys(kwargs).includes('context')) {
      kwargs2.context = this.env.context
    }

    const payload = { model: this._name, method, args, kwargs: kwargs2 }
    return this._odoo.web.dataset.call_kw(payload)
  }

  static async execute(method, ...args) {
    return this.execute_kw(method, args, {})
  }

  static async web_search_read(kwargs = {}) {
    return this.execute_kw('web_search_read', [], kwargs)
  }

  static async search_read(kwargs = {}) {
    return this.execute_kw('search_read', [], kwargs)
  }

  static async read(ids, kwargs = {}) {
    const method = 'read'
    const { fields = [] } = kwargs
    const kwargs2 = { ...kwargs }
    delete kwargs2.fields
    return this.execute_kw(method, [ids, fields], kwargs2)
  }

  static async write(rid, vals) {
    const method = 'write'
    return this.execute(method, rid, vals)
  }

  static async create(vals) {
    return this.execute('create', vals)
  }

  static async unlink(rid) {
    const method = 'unlink'
    return this.execute(method, rid)
  }

  static async default_get(fields) {
    return this.execute('default_get', fields)
  }

  static async name_search(kwargs = {}) {
    // const { name, args, operator, limit } = kwargs
    return this.execute_kw('name_search', [], kwargs)
  }

  static async name_get(ids) {
    return this.execute('name_get', ids)
  }

  static async onchange(ids, values, field_name, field_onchange) {
    const session_info = this._odoo.session_info
    const server_version_info = session_info.server_version_info
    const version = server_version_info[0]
    const is_call_default =
      (!field_name || (Array.isArray(field_name) && !field_name.length)) &&
      version == 13

    // console.log(version, is_call_default)
    if (is_call_default) {
      return this.default_get_onchange(values, field_onchange)
    } else {
      return this.execute('onchange', ids, values, field_name, field_onchange)
    }
  }

  static async default_get_onchange(values, field_onchange) {
    const fields = Object.keys(field_onchange).filter(
      fld => fld.split('.').length === 1
    )

    // console.log([field_onchange, fields])

    const default_get1 = await this.default_get(fields)

    const _get_default = col => {
      const meta = this._fields[col]

      if (['many2many'].includes(meta.type)) {
        return [[6, false, []]]
      } else if (['one2many'].includes(meta.type)) {
        return []
      } else if (['float', 'integer', 'monetary'].includes(meta.type)) {
        return 0
      } else if (['text', 'html'].includes(meta.type)) {
        return ''
      }
      return false
    }

    const values_onchange2 = fields.reduce((acc, cur) => {
      acc[cur] = _get_default(cur)
      return acc
    }, {})

    const values_onchange = { ...values_onchange2, ...values, ...default_get1 }

    const field_name = fields
    const args = [[], values_onchange, field_name, field_onchange]
    const onchange = await this.execute('onchange', ...args)

    // console.log(fields, default_get1, onchange)

    // console.log('default get 2', onchange)

    // # TBD: default_get 里面 可能有 m2o o2m 需要处理
    // default_get, m2o 返回值 是 id, 需要 补充上 display_name
    const default_get2 = {}

    for (const col of Object.keys(default_get1)) {
      const meta = this._fields[col]
      if (meta.relation && meta.type === 'many2one') {
        const ref_val = default_get1[col]

        if (ref_val) {
          const ref_ids = Array.isArray(ref_val) ? ref_val : [ref_val]
          const domain = [['id', 'in', ref_ids]]
          const ref_records = await this.env
            .model(meta.relation)
            .execute_kw('name_search', [], { args: domain })

          const ref_rec = ref_records[0]

          default_get2[col] = [...ref_rec]
        } else {
          default_get2[col] = default_get1[col]
        }
      } else {
        default_get2[col] = default_get1[col]
      }
    }

    // const values_ret = { ...values, ...default_get2, ...onchange.value }
    const values_ret = { ...default_get2, ...onchange.value }
    const onchange2 = { ...onchange, value: values_ret }

    // console.log('default get 3', onchange2)

    return onchange2
  }
}

MetaModel._env = undefined
MetaModel._odoo = undefined
MetaModel._model = undefined

const _normalize_ids = ids => {
  if (!ids) {
    return []
  }

  if (Array.isArray(ids)) {
    return [...ids]
  }

  return [ids]
}

class BaseModel extends MetaModel {
  constructor(payload = {}) {
    super()
    const { fields, from_model, from_field } = payload
    this._fields = fields

    this._from_model = from_model
    this._from_field = from_field

    this._values = {}
    this._values_to_write = {}
    this._store_relations = {}

    this._columns = this._init_columns(fields)

    this._ids = []
  }

  copy() {
    console.log('copy MetaModel:')
    const rec = new this.constructor(this._ids)
    return rec
  }

  get from_model() {
    return this._from_model
  }

  get from_field() {
    return this._from_field
  }

  _init_columns(fields) {
    return Object.keys(fields).reduce((acc, fld) => {
      acc[fld] = new Field({ record: this, name: fld })
      return acc
    }, {})
  }

  get fields() {
    return this._fields
  }

  get columns() {
    // 所有的 字段, 数据初始化时 设置
    return this._columns
  }

  get ids() {
    return this._ids
  }

  static async browse(ids, { fields, from_model, from_field }) {
    // TBD,
    // if fields is array: be call from not viewmode
    //    then: need get fields.meta by fields_get()
    // if fields is array: be call from viewmode
    //    then: fields.meta is here

    const fields_list = Object.keys(fields)
    // const all = await this.search_read({ fields: fields_list })
    // console.log(all)
    const res = await this.read(ids, { fields: fields_list })
    const record = new this({ fields, from_model, from_field })

    record._init_data(res)
    return record
  }

  _init_data(res) {
    this._ids = _normalize_ids(res.map(item => item.id))
    Object.keys(this.columns).forEach(item => {
      const col = this.columns[item]
      res.forEach(one => {
        col.set_by_init(one.id, one[col.fname])
      })
    })
  }

  set_values_by_onchange(rid, vals) {
    // 仅仅 被 parent 调用
    // parent 其他 字段 onchange 之后, 更新 o2m字段时, 走到这里

    if (!this._ids.includes(rid)) {
      this._ids = [...this._ids, rid]
    }

    Object.keys(vals).forEach(item => {
      const col = this.columns[item]
      if (col) {
        col.set_value_by_onchange(rid, vals[item])
      } else {
        throw `${item} is undefined in columns`
      }
    })
  }

  get_values(rid) {
    const values_init = { id: rid }
    return Object.keys(this.columns).reduce((acc, cur) => {
      const col = this.columns[cur] || {}
      const values = col.get_values_display(rid) || {}
      acc = { ...acc, ...values }
      return acc
    }, values_init)
  }

  get_values_onchange(rid, for_parent) {
    // 给 onchange 事件准备参数用的 数据
    return Object.keys(this.columns).reduce((acc, cur) => {
      // console.log('sssss,', this, cur, this.fields, this.columns)
      const col = this.columns[cur]
      return { ...acc, [cur]: col.get_value_onchange(rid, for_parent) }
    }, {})
  }

  get_values_for_write(rid) {
    // 给 create/write 准备参数用的 数据
    return Object.keys(this.columns).reduce((acc, cur) => {
      const col = this.columns[cur]
      const value = col.get_value_for_write(rid)
      if (value !== undefined) {
        acc[cur] = value
      }
      return acc
    }, {})
  }
}

class TreeModel extends BaseModel {
  constructor(payload = {}) {
    super(payload)
    this._total_length = 0
  }

  get total_length() {
    return this._total_length
  }

  get values_list() {
    return this.ids.map(item => this.get_values(item))
  }

  static async search_browse(payload = {}) {
    const { fields } = payload
    // TBD,
    // if fields is array: be call from not viewmode
    //    then: need get fields.meta by fields_get()
    // if fields is array: be call from viewmode
    //    then: fields.meta is here

    const fields_list = Object.keys(fields)
    const paylaod2 = { ...payload, fields: fields_list }
    const result = await this.web_search_read({ ...paylaod2 })
    const rec = new this({ fields })
    const { length, records } = result
    rec._init_data(records)
    rec._total_length = length
    return rec
  }
}

class FormModel extends TreeModel {
  constructor(payload = {}) {
    super(payload)
  }

  get id() {
    return this._ids.length ? this._ids[0] : null
  }

  get values() {
    return this.get_values(this.id)
  }

  get values_onchange() {
    return this.get_values_onchange(this.id)
  }

  get values_onchange_for_parent() {
    return this.get_values_onchange(this.id, true)
  }

  get values_for_write() {
    return this.get_values_for_write(this.id)
  }

  async relation_browse(fname, kwargs = {}) {
    // m2m 字段 name_get
    // const kwargs2 = {  context }
    // console.log('relation_browse', fname)
    const field = this.columns[fname]
    // console.log('relation_browse', fname, this.columns, field)
    return await field.relation_browse(this.id, kwargs)
  }

  get_selection(fname, kwargs) {
    // const kwargs2 = { args, name: query, operator, limit, context }
    const field = this.columns[fname]
    return field.get_selection(kwargs)
  }

  static async new_and_onchange({ fields, field_onchange }) {
    // TBD,
    // if fields is array: be call from not viewmode
    //    then: need get fields.meta by fields_get()
    // if fields is array: be call from viewmode
    //    then: fields.meta is here
    // const fields_list = Object.keys(fields)

    const _get_default = col => {
      const meta = fields[col]

      if (['many2many'].includes(meta.type)) {
        return [[6, false, []]]
      } else if (['one2many'].includes(meta.type)) {
        return []
      } else if (['float', 'integer', 'monetary'].includes(meta.type)) {
        return 0
      } else if (['text', 'html'].includes(meta.type)) {
        return ''
      }
      return false
    }

    const record = new this({ fields })
    record._ids = []

    const rid = record.id
    Object.keys(record.columns).forEach(item => {
      const col = record.columns[item]
      const val = _get_default(item)
      col.set_value_by_onchange(rid, val)
    })

    const args = [[], {}, '', field_onchange]
    const res = await this.onchange(...args)
    const onchange_value = res.value
    record._after_onchange(onchange_value)
    return record
  }

  async set_and_onchange(fname, value, kwargs = {}) {
    const { text, field_onchange } = kwargs
    const field = this.columns[fname]
    field.set_value(this.id, value, text)

    // fro debug, always call onchange
    // const to_call = field_onchange[fname]
    const to_call = field_onchange[fname] || true
    if (to_call) {
      const ids = this.id && !is_virtual_id(this.id) ? [this.id] : []
      const values = this.values_onchange

      if (this.from_model) {
        const parent_vals = this.from_model.values_onchange_for_parent
        const from_col = this.from_model.columns[this.from_field]
        const relation_field = from_col.meta.relation_field
        // console.log(this, relation_field)
        values[relation_field] = parent_vals
      }

      const args = [ids, values, fname, field_onchange]
      const res = await this.constructor.onchange(...args)
      const onchange_value = res.value
      this._after_onchange(onchange_value)
    }

    return this.values
  }

  _after_onchange(one) {
    console.log(one)
    Object.keys(one).forEach(fld => {
      const col = this._columns[fld]
      if (col) {
        col.set_value_by_onchange(this.id, one[fld])
      } else {
        // console.log(fld, one)
        console.log(
          'Trow Error.',
          `${fld} in onchange.values, but not init in this.columns`
        )
        // throw `${fld} in onchange.values, but not init in this.columns`
      }
    })
  }

  async commit() {
    const values = this.values_for_write
    console.log(this.id, values)
    if (this.id) {
      if (Object.keys(values).length) {
        await this.constructor.write(this.id, values)
        const fields = Object.keys(this.fields)
        const res = await this.constructor.read(this.id, { fields })
        // console.log(res)
        this._after_commit(res)
      }
      return this.values
    } else {
      // TBD new
      const rid = await this.constructor.create(values)
      const fields = Object.keys(this.fields)
      const res = await this.constructor.read(rid, { fields })
      // console.log(res)
      this._after_commit(res)
      return this.values
    }
  }

  _after_commit(res) {
    this._init_data(res)
    const keys = Object.keys(this._values_to_write)
    keys.forEach(key => {
      delete this._values_to_write[key]
    })
  }
}

export const Model = FormModel
