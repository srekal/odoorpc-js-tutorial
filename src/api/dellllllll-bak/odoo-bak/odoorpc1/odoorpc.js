import request from './request'
import { WEB } from './web'
import { Environment } from './env'

export class ODOO {
  constructor(payload) {
    const { baseURL, timeout = 50000 } = payload
    this._rpc = new request.ProxyJSON({ baseURL, timeout })
    this._web = new WEB({ odoo: this })

    this._env = new Environment({ odoo: this })

    this._version_info = {}
    const version_info = this.get_version_info()
    this._version_info_promise = version_info
    version_info.then(res => {
      this._version_info = res
    })

    this._virtual_id = 1
  }

  get_virtual_id() {
    // new a o2m field, need an unique virtual id
    const int_virtual_id = this._virtual_id
    this._virtual_id = this._virtual_id + 1
    return `virtual_${int_virtual_id}`
  }

  get web() {
    return this._web
  }

  get env() {
    return this._env
  }

  get session() {
    return this.web.session
  }

  get session_info() {
    return this.web.session.session_info
  }

  get version_info() {
    return this._version_info
  }

  get version_info_promise() {
    return this._version_info_promise
  }

  async json_call(url, payload = {}) {
    const data = await this._rpc.call(url, payload)
    if (data.error) {
      throw data.error
    } else {
      return data.result
    }
  }

  async get_version_info() {
    const url = '/web/webclient/version_info'
    return await this.json_call(url, {})
  }

  async login({ db, login, password }) {
    const res = await this.web.session.authenticate({ db, login, password })
    return res
  }

  async logout() {
    try {
      await this.web.session.destroy()
      return true
    } catch {
      return true
    }
  }

  async session_check() {
    try {
      if (this.session_info.uid) {
        await this.web.session.check()
      } else {
        await this.web.session.get_session_info()
      }
      return true
    } catch (erorr) {
      return false
    }
  }
}
