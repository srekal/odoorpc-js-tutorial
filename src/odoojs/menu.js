const menuLoadodoo = process.env.VUE_APP_MENU_LOAD_ODOO

const menuLoad = process.env.VUE_APP_MENU_LOAD

export class Menu {
  constructor(env) {
    this._env = env
    this._menus = []

    this._actions = {}
  }

  get env() {
    return this._env
  }

  get menus() {
    return this._menus
  }

  get actions() {
    return Object.keys(this._actions).map(item => {
      return { name: item }
    })
  }

  async load_menus() {
    const odoojs_menus = await this._load_menus_odoojs()
    const odoo_menus = await this._load_menus_odoo()
    const subMenus = [...odoo_menus, ...odoojs_menus]
    console.log(odoojs_menus)
    this._menus = subMenus
    return subMenus
  }

  async _load_menus_odoojs() {
    if (!menuLoad) {
      return []
    }

    const get_root = async () => {
      try {
        const root = await this.env.ref('odoojs_menu.menu_odoojs')
        // console.log(root)
        return root
      } catch {
        return null
      }
    }

    const root = await get_root()

    if (!root) {
      return []
    }

    const odoojs_menus = await this._load_menus_by_root(root.id)

    console.log('odoojs_menus', JSON.parse(JSON.stringify(odoojs_menus)))
    return odoojs_menus
  }

  async _load_menus_odoo() {
    if (!menuLoadodoo) {
      return []
    }

    const odoo_menus = await this._load_menus_by_root()

    console.log('odoo_menus', JSON.parse(JSON.stringify(odoo_menus)))

    const odoo_root = {
      icon: 'ios-apps',
      id: 0,
      name: 'odoo_root',
      title: 'odoo官方菜单',
      children: odoo_menus
    }

    return [odoo_root]
  }

  async _load_menus_by_root(root_id) {
    const Model = this.env.model('ir.ui.menu')

    const domain = root_id ? [['id', 'child_of', root_id]] : []
    const res = await Model.search_read({ domain })
    // console.log(res)

    const get_sub = root_id => {
      const my_filter = item =>
        root_id
          ? item.parent_id && item.parent_id[0] === root_id
          : item.parent_id === false

      return res.filter(my_filter).map(item => {
        if (item.action) {
          // ir.actions.act_window
          // ir.actions.act_window

          const [action_type, action_id] = item.action.split(',')

          return {
            id: item.id,
            type: action_type,
            name: action_id,
            title: item.name,
            item
          }
        } else {
          const children = get_sub(item.id)

          return {
            id: item.id,
            name: `ir.ui.menu,${item.id}`,
            title: item.name,
            icon: item.iview_icon,
            children,
            item
          }
        }
      })
    }

    const subMenus = get_sub(root_id)

    // console.log(subMenus)

    const actions = res
      .filter(item => item.action)
      .reduce((acc, item) => {
        const action_id = item.action.split(',')[1]
        return { ...acc, [action_id]: action_id }
      }, {})

    this._actions = { ...this._actions, ...actions }

    // .map(item => {
    //   const action_id = item.action.split(',')[1]
    //   return { name: action_id }
    // })

    return subMenus
  }
}
