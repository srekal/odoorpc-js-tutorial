import Test from './test'

const baseURL = process.env.VUE_APP_BASE_API
const master_pwd = 'admin'
// const login_info = { db: 'test_db', login: 'admin', password: '123456' }
const login_info = {
  db: 'test_account_0521',
  login: 'admin',
  password: '123456'
}

const config = { baseURL, master_pwd, login_info }
const test = new Test(config)

export const test_rpc = async () => {
  // const childs = { ss: 1 }
  // console.log(childs)
  // for (const ch of Object.keys(childs)) {
  //   console.log(ch)
  // }
  // for (const ch in childs) {
  //   console.log(ch)
  // }
  // await test.base.test_call_odoo()
  // await test_session()
  // await test_dataset()
  // await test_context()
  // await test_ir_module()
  await test_o2m_edit()
}

const test_o2m_edit = async () => {
  await test.o2m.edit()
}

const test_ir_module = async () => {
  await test.ir_module.list_installed()
  await test.ir_module.install()
  // await test.ir_module.uninstall()
}

const test_context = async () => {
  await test.context.test_with_context()
  await test.context.test_with_env()
  await test.context.test_env_copy()
}

const test_dataset = async () => {
  await test.dataset.call_kw()
  await test.model.name_search()
  await test.model.search_read()
}

const test_session = async () => {
  await test.session.authenticate()
  await test.session.get_session_info()
  await test.session.check()
  await test.session.destroy()
  await test.session.get_lang_list()
  await test.session.modules()
  await test.session.change_password()
}

const test_database = async () => {
  //   await test.database.list()
  //   await test.database.create()
  //   await test.database.list()
  //   await test.database.drop()
  //   await test.database.list()
}
