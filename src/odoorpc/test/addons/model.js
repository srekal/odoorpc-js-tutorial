import { LoginTestCase } from './base'

export default class ModelTestCase extends LoginTestCase {
  async name_search() {
    await this.login()
    const model = 'ir.module.module'
    const Model = this.api.env.model(model)
    const limit = 80
    const res = await Model.name_search({ limit })
    console.log(res)
  }

  async search_read() {
    await this.login()
    const model = 'ir.module.module'
    const domain = []
    const fields = ['name']
    const limit = 80
    const kwargs = { domain, fields, limit }
    const Model = this.api.env.model(model)
    const res = await Model.search_read(kwargs)

    console.log(res)
  }
}
