import axios from 'axios'

class ProxyJSON {
  constructor(payload) {
    const { baseURL, timeout = 50000 } = payload
    this._baseURL = baseURL
    this._timeout = timeout
    this._service = this._get_service()
  }

  _get_service() {
    const service = axios.create({
      baseURL: this._baseURL,
      // withCredentials: true,
      timeout: this._timeout
    })

    const that = this

    service.interceptors.request.use(
      config => {
        // console.log('sid:', that._sid)
        if (that._sid) {
          config.headers['X-Openerp-Session-Id'] = that._sid
        }
        return config
      },
      error => {
        return Promise.reject(error)
      }
    )

    service.interceptors.response.use(
      response => {
        const url = response.config.url
        const url_auth = '/web/session/authenticate'
        const url_info = '/web/session/get_session_info'

        if (url === url_auth || url === url_info) {
          // if run test or run from wx miniprograme, not cookie,
          // so wo set sid to call odoo

          const headers = response.headers
          const cookie = headers['set-cookie']
          if (cookie) {
            const cookie2 = cookie[0]
            const session_id = cookie2.slice(11, 51)
            that._sid = session_id
          }
        }

        const res = response.data
        // console.log(response)
        return res
      },
      error => {
        return Promise.reject(error)
      }
    )

    return service
  }

  async call(url, payload = {}) {
    const url2 = url[0] === '/' ? url : `/${url}`

    const data = {
      jsonrpc: '2.0',
      method: 'call',
      params: payload,
      id: Math.floor(Math.random() * 1000000000 + 1)
    }

    const response = await this._service({
      url: url2,
      method: 'post',
      data
    })

    return response
  }
}

export default { ProxyJSON }
