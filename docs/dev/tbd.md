### 流程

#### read, 查询数据

1. listview read 数据
2. pick one id
3. formview read(id) 数据
4. init columns
5. o2m.ids inited in columns
6. o2m browse_read
7. o2m treeviewmodel init

#### edit, 编辑数据 o2m

1. read, o2m read
2. editable
3. set o2m, 拆分为 new / update / delete
4. after_onchange o2m , 同 set o2m
5. 本模型, 1_value 原值 ids, 只服务于 relation read
6. 本模型, 1_value_wr 存储 o2m 的 更新 [[0,id,]]
7. 本模型的 value 服务于显示
8. 子模型的 1_value, 对应 父模型 的 [[4,id]] 的数据
9. 子模型的 1_value_wr, 对应 父模型 的 [[0/1,id]] 的数据
10. values_onchange 从本模型取 1_value_wr, 从 子模型补充数据
11. values_wr 从本模型取 1_value_wr, 从 子模型补充数据

### read, dataDict

1. 读取结果 放在 dataDict 里
2. 直接 raw
3. m2o [id, name]
4. m2m [ ids ], [{}]
5. o2m [ ids ], [{}]
6. 子 o2m , [ ids ], [{id, o2m: [ids], o2m_record: [{}] }]
7. 结果更新到 values
8. 嵌套读取结果 也更新到 values

### display === values

1. values = dataDict + values_to_write
2. m2o, m2o.name
3. m2m, m2m.records
4. o2m, o2m.records
5. o2m = [...ids, ...oldids, ...vids ]
6. o2m.records = [{id, m2o, m2m, o2m}]
7. o2m.m2o = {m2o:id, m2o.name: '' }
8. o2m.m2m = {m2m: [ids], m2m.records: {} }
9. o2m.o2m = {o2m: [ids], o2m.records: {} }

### to write, values_to_write

1. values_to_write
2. 页面编辑 更新到 values_to_write
3. onchange 返回结果 更新到 values_to_write
4. m2o, m2o.name
5. m2m, [[6,0,ids],[4,id],[3,id]], 应该更新 display
6. o2m, [[4,id],[1,id,{}],[2,id],[0,vid,{}]], 应该更新 display
7. 嵌套 o2m

#### to write, 嵌套 o2m

1. [4,id], [2,id] , 无嵌套
2. [1,id,{}],[0,vid,{}], 有嵌套
3. [x, id, {m2o, m2m:, o2m }]
4. m2o = 更新到 display
5. m2m = 更新到 display
6. o2m = 更新到 display

### onchange, values_to_onchange

#### onchange root

1. 判断 field_onchange 是否 触发
2. 新增时 无 id, 编辑时 有 id
3. 普通字段, 取 raw 值
4. m2m 字段, 直接取 values_to_write. [[6,0,old_ids],[4,id],[3,id]]
5. o2m 字段, 取 values_to_write. [[4,id],[1,id,{}],[2,id],[0,vid,{}]]
6. 其中 [1,id,{}],[0,vid,{}], 嵌套部分, 需要 从 display 中补充, 但是 不需要 id

#### onchange 嵌套 o2m

1. [4,id], [2,id] , 无嵌套
2. [1,id,{}],[0,vid,{}], 有嵌套, 需要 从 display 中补充,
3. [x, id, {m2o, m2m: [[6,0,ids],[4,id],[3,id]], o2m:[[4,id]] }]
4. m2o = 直接取自 display,
5. m2m = 直接取, 无需 从 display 补充
6. o2m = 直接取, 从 display 嵌套补充
7. 这里不需要 o2m id

### onchange 返回值

1. 更新到 values_to_write
2. values_to_edit 是计算结果

### value for write

1. 直接取自 values_to_write
2. 过滤掉 readonly 的字段

### o2m 子表 onchange

1. 与正常到 onchange 相同
2. 额外需要一个 relation_field 的值
3. relation_field 是父表,
4. 父表普通字段, 直接取 display 值
5. 父表的 m2m 字段, 直接取 to_write 值
6. 父表的 o2m 字段, 直接取 to_write 值

###

action_partner_employee_form 的 context 及 domain 有值
