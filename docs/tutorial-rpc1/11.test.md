### 整理下测试代码

1. 测试代码越来越多, 需要归类整理下
2. 把测试代码放在一个文件夹中, 便于管理
3. 创建文件夹 /odoorpc/test
4. 创建文件夹 /odoorpc/test/addons
5. 创建文件 /odoorpc/test/index.js
6. 创建文件 /odoorpc/test/addons/base.js
7. 创建文件 /odoorpc/test/addons/database.js
8. 修改 /odoorpc/test_rpc.js

#### /odoorpc/test/index.js, 内容如下

1. 这个文件的作用是把 odoorpc/test/addons 下的文件自动引入

```
const AllFiles = require.context('./addons', true, /\.js$/)

const AllTest = AllFiles.keys().reduce((models, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = AllFiles(modulePath)
  models = { ...models, [moduleName]: value.default }
  return models
}, {})

// console.log(AllTest)

export default class Test {
  constructor(config) {
    Object.keys(AllTest).forEach(item => {
      this[item] = new AllTest[item](config)
    })
  }
}

```

2. 以上的写法 等价于

```
import base from './addons/base'
import database from './addons/database'
const AllTest = { web, database }
// ...

```

3. 但是后面的这种写法, 每当 addons 下增加新文件时, 需要修改 index.js
4. 前面的第一种写法, 每当 addons 下增加新文件时, 无需要修改 index.js

#### /odoorpc/test/addons/base.js, 内容如下

```
import { ODOO } from '@/odoojs'

export class BaseTestCase {
  constructor({ baseURL }) {
    this.baseURL = baseURL
    this.api = new ODOO({ baseURL })
  }

  async test_call_odoo() {
    const res = await this.api.get_version_info()
    console.log(res)
  }
}

export default BaseTestCase

```

#### /odoorpc/test/addons/database.js, 内容如下

```
import { BaseTestCase } from './base'

export default class DatabaseTestCase extends BaseTestCase {
  constructor(config) {
    super(config)
    const { master_pwd } = config
    this.master_pwd = master_pwd
  }

  async list() {
    return await this.api.web.datebase.list()
  }

  async create() {
    const master_pwd = this.master_pwd
    const name = 'db_for_test_create_drop'
    const lang = 'zh_CN'
    const password = '123456'
    const kwargs = {
      demo: false,
      login: 'admin',
      country_code: 'CN',
      phone: ''
    }

    const database_list = await this.api.web.datebase.list()

    if (database_list.includes(name)) {
      console.log('database: ', name, 'is exist')
      return
    }

    const payload = [master_pwd, name, lang, password, kwargs]
    const res = await this.api.web.datebase.create(...payload)
    return res
  }

  async drop() {
    const master_pwd = this.master_pwd
    const name = 'db_for_test_create_drop'

    const database_list = await this.api.web.datebase.list()

    if (!database_list.includes(name)) {
      console.log('database: ', name, 'is not exist')
      return
    }

    const res = await this.api.web.datebase.drop(master_pwd, name)
    return res
  }
}

```

#### /odoorpc/test_rpc.js 内容如下

```
import Test from './test'

const baseURL = process.env.VUE_APP_BASE_API

const master_pwd = 'admin'
const config = { baseURL, master_pwd }
const test = new Test(config)

export const test_rpc = async () => {
  await test.base.test_call_odoo()
  await test.database.list()
  // await test.database.create()
  // await test.database.drop()
}


```

#### 验证整理后的测试代码正确

1. 运行测试函数
2. 验证测试代码正确
